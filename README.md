# Java Monocle EPD

This project is a class library containing the Java implementation of a Monocle platform called “EPD” for devices with an electrophoretic display.
The initial support is for the Kobo Touch N905B and N905C e-readers with a Freescale i.MX507 processor, Electrophoretic Display Controller, and E Ink Pearl screen.

## Project

The NetBeans project uses a Java platform called “JDK 1.8 for x86egl” that contains the Oracle JDK 8 installation with the OpenJFX x86egl bundle overlay.

## Installation

Place the class library on the target device in `$HOME/lib/ext/jfxepd.jar`.

Add the Java system property `-Djava.ext.dirs=$HOME/lib/ext:$JAVA_HOME/jre/lib/ext` to pick up the new class library, where `JAVA_HOME` is set to the location of your JDK 8 installation with the OpenJFX overlay bundle.

Also install the native C implementation in [glass_monocle_epd](https://gitlab.com/openjfxepd/glass_monocle_epd).

