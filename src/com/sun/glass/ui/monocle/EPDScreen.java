/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.sun.glass.ui.monocle;

import com.sun.glass.ui.Pixels;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB_PRE;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.AccessController;
import java.security.PrivilegedAction;
import javax.imageio.ImageIO;

/**
 * EPDScreen represents the Electrophoretic Display screen. This class transfers
 * the display content from a 32-bit ARGB Java frame buffer to the 16-bit RGB565
 * Electrophoretic Display Controller frame buffer, sends updates to the
 * display, and waits for updates to complete.
 *
 * @author John Neffenger
 */
public class EPDScreen implements NativeScreen {

    private static final String FB_DEV_KEY = "monocle.screen.fb";
    private static final String FB_DEV_DEFAULT = "/dev/fb0";

    private static final int NATIVE_FORMAT = Pixels.Format.BYTE_BGRA_PRE;
    private static final int DPI = 96;
    private static final float SCALE = 1.0f;

    private static final int BYTES_32BITS = 4;

    protected int depth;
    protected int nativeFormat;
    protected int width;
    protected int height;
    protected int dpi;
    protected long nativeHandle;
    protected float scale;

    private final String fbDevPath;
    private final EPDFrameBuffer epdFB;
    private final Framebuffer fb;
    private FileChannel fbdev;
    private boolean isShutdown;

    public EPDScreen() {
        fbDevPath = AccessController.doPrivileged((PrivilegedAction<String>) ()
                -> System.getProperty(FB_DEV_KEY, FB_DEV_DEFAULT));
        try {
            epdFB = new EPDFrameBuffer(fbDevPath);
            epdFB.init();
            depth = epdFB.getDepth();
            nativeFormat = NATIVE_FORMAT;
            width = epdFB.getWidth();
            height = epdFB.getHeight();
            dpi = DPI;
            nativeHandle = epdFB.getNativeHandle();
            scale = SCALE;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        // The Framebuffer class stores its contents in a buffer with a 32-bit
        // color depth but can write to targets with a 32-bit or 16-bit color
        // depth. FramebufferPlusY8 extends the Framebuffer class to support
        // 8-bit targets.
        // Source Java buffer:
        //   32-bit:  8-bit ARGB color components (4 bytes/pixel)
        // Target EPD framebuffers:
        //   16-bit:  RGB565 color components with no alpha (2 bytes/pixel)
        //    8-bit:  Y8 grayscale component (1 byte/pixel)
        ByteBuffer bb = ByteBuffer.allocateDirect(width * height * BYTES_32BITS);
        bb.order(ByteOrder.nativeOrder());
        fb = new FramebufferPlusY8(bb, width, height, depth, true);
        System.out.println("Byte buffer has backing array = " + bb.hasArray());
        System.out.println("Byte buffer is direct = " + bb.isDirect());
    }

    private void open() throws IOException {
        if (fbdev == null) {
            Path path = FileSystems.getDefault().getPath(fbDevPath);
            fbdev = FileChannel.open(path, StandardOpenOption.WRITE);
        }
    }

    private void close() {
        if (fbdev != null) {
            try {
                fbdev.close();
            } catch (IOException e) {
            }
            fbdev = null;
        }
        epdFB.close();
    }

    private void writeScreenCapture(String name, int count, IntBuffer buffer) {
        try {
//            InputStream stream = new ByteArrayInputStream(fb.getBuffer().array());
//            Image fxImage = new Image(stream);
            BufferedImage awtImage = toAWTImage(buffer);
            String suffix = String.format("%04d", ++count);
            ImageIO.write(awtImage, "png", new File(suffix + name + ".png"));
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    private BufferedImage toAWTImage(IntBuffer b) {
        int w = 800;
        int h = 600;
        BufferedImage awtImage = new BufferedImage(w, h, TYPE_INT_ARGB_PRE);
        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                awtImage.setRGB(x, y, b.get(y * w + x));
            }
        }
        return awtImage;
    }

    @Override
    public int getDepth() {
        return depth;
    }

    @Override
    public int getNativeFormat() {
        return nativeFormat;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getDPI() {
        return dpi;
    }

    @Override
    public long getNativeHandle() {
        return nativeHandle;
    }

    @Override
    public void shutdown() {
        fb.clearBufferContents();
        if (fbdev != null) {
            swapBuffers();
            close();
        }
        isShutdown = true;
    }

    @Override
    public void uploadPixels(Buffer b, int x, int y, int width, int height, float alpha) {
        fb.composePixels(b, x, y, width, height, alpha);

        // DEBUG
        if (b.hasRemaining()) {
            System.err.println("Buffer was clobbered: " + b.remaining() + " elements remaining");
        }
    }

    private int marker = 0;

    // Synchronized for shutdown on "Monocle shutdown hook" thread
    @Override
    public synchronized void swapBuffers() {
        try {
            if (!isShutdown && fb.hasReceivedData()) {
                open();
                fbdev.position(0);
                fb.write(fbdev);
                if (marker != 0) {
                    epdFB.waitUpdate(marker);
                }
                marker = epdFB.sendUpdate();

//                epdFB.sync();
//                uploadCount++;
//                if (uploadCount % 32 == 0) {
//                    writeScreenCapture("A", uploadCount, (IntBuffer) saveBuffer);
//                }
//                if (uploadCount % 32 == 0) {
//                    writeScreenCapture("B", uploadCount, fb.getBuffer().asIntBuffer());
//                }
            }
        } catch (IOException e) {
            System.err.println("Error writing to framebuffer: " + e);
        } finally {
            fb.reset();
        }
    }

    @Override
    public ByteBuffer getScreenCapture() {
        return fb.getBuffer();
    }

    @Override
    public float getScale() {
        return scale;
    }

    @Override
    public String toString() {
        return fbDevPath + " " + width + " px × " + height + " px × "
                + depth + " bits @ " + dpi + " ppi";
    }
}
