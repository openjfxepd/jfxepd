/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.sun.glass.ui.monocle;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author John Neffenger
 */
class EPDInputDeviceRegistry extends InputDeviceRegistry {

    /*
        /dev/input/event0
           bustype : BUS_HOST
           vendor  : 0x0
           product : 0x0
           version : 0
           name    : "mxckpd"
           bits ev : EV_SYN EV_KEY

        /dev/input/event1
           bustype : BUS_I2C
           vendor  : 0x0
           product : 0x0
           version : 0
           name    : "zForce-ir-touch"
           bits ev : EV_SYN EV_KEY EV_ABS

        mxckpd
        linux-2.6.35.3/drivers/input/keyboard/mxc_keyb.c
            scancode = keycode  # keyname
            0x0000 = 102  # KEY_HOME
            0x0001 = 116  # KEY_POWER
        Event: time 1459442977.698975, type 1 (EV_KEY), code 116 (KEY_POWER), value 1
        Event: time 1459442977.958963, type 1 (EV_KEY), code 116 (KEY_POWER), value 0
        Event: time 1459442981.379041, type 1 (EV_KEY), code 102 (KEY_HOME), value 1
        Event: time 1459442981.589015, type 1 (EV_KEY), code 102 (KEY_HOME), value 0

        Note: Driver does not call input_sync for EV_SYN SYN_REPORT events.
        linux-2.6.35.3/include/linux/input.h
        static inline void input_sync(struct input_dev *dev)
        {
                input_event(dev, EV_SYN, SYN_REPORT, 0);
        }

        zForce-ir-touch
        linux-2.6.35.3/drivers/input/touchscreen/zforce_i2c.c
            bits: BTN_2
            bits: BTN_TOUCH
        Event: time 1459443720.942624, type 3 (EV_ABS), code 1 (ABS_Y), value 415
        Event: time 1459443720.942652, type 3 (EV_ABS), code 0 (ABS_X), value 475
        Event: time 1459443720.942662, type 3 (EV_ABS), code 24 (ABS_PRESSURE), value 100
        Event: time 1459443720.942674, type 1 (EV_KEY), code 330 (BTN_TOUCH), value 1
        Event: time 1459443720.942682, -------------- SYN_REPORT ------------
        Event: time 1459443720.943860, type 3 (EV_ABS), code 24 (ABS_PRESSURE), value 0
        Event: time 1459443720.943868, type 1 (EV_KEY), code 330 (BTN_TOUCH), value 0
        Event: time 1459443720.943873, -------------- SYN_REPORT ------------
     */
    EPDInputDeviceRegistry(boolean headless) {
        Map<File, LinuxInputDevice> deviceMap = new HashMap<>();
        UdevListener udevListener = (action, event) -> {
            String subsystem = event.get("SUBSYSTEM");
            String devPath = event.get("DEVPATH");
            String devName = event.get("DEVNAME");
            if (subsystem != null && subsystem.equals("input")
                    && devPath != null && devName != null) {
                try {
                    File sysPath = new File("/sys", devPath);
                    if (action.equals("add")
                            || (action.equals("change") && !deviceMap.containsKey(sysPath))) {
                        File devNode = new File(devName);
                        LinuxInputDevice device = createDevice(devNode, sysPath, event);
                        if (device != null) {
                            deviceMap.put(sysPath, device);
                        }
                    } else if (action.equals("remove")) {
                        LinuxInputDevice device = deviceMap.get(sysPath);
                        deviceMap.remove(sysPath);
                        if (device != null) {
                            devices.remove(device);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Udev.getInstance().addListener(udevListener);
        // Request updates for existing devices
        SysFS.triggerUdevNotification("input");
    }

    private LinuxInputDevice createDevice(File devNode, File sysPath, Map<String, String> udevManifest) throws IOException {
        LinuxInputDevice device = new LinuxInputDevice(devNode, sysPath, udevManifest);
        return addDeviceInternal(device, "Linux input: " + devNode.toString());
    }

    // This method short-circuits the LinuxInputDeviceRegistry method to force
    // the correct input processor.
    //
    // On Ubuntu 14.04.4 with udev version 204 we get:
    //    $ sudo udevadm info -q all -n /dev/input/event1
    //    P: /devices/virtual/input/input1/event1
    //    N: input/event1
    //    E: DEVNAME=/dev/input/event1
    //    E: DEVPATH=/devices/virtual/input/input1/event1
    //    E: ID_INPUT=1
    //    E: ID_INPUT_TOUCHSCREEN=1
    //    E: MAJOR=13
    //    E: MINOR=65
    //    E: SUBSYSTEM=input
    //    E: USEC_INITIALIZED=15773353426
    //
    // But on the Kobo Touch side where udev version 142 is running, we get:
    //    # udevadm info -q all -n /dev/input/event1
    //    P: /devices/virtual/input/input1/event1
    //    N: input/event1
    //    S: char/13:65
    //    E: UDEV_LOG=3
    //    E: DEVPATH=/devices/virtual/input/input1/event1
    //    E: MAJOR=13
    //    E: MINOR=65
    //    E: DEVNAME=/dev/input/event1
    //    E: DEVLINKS=/dev/char/13:65
    //
    // This older version causes the device.isTouch() method to return false
    // because it can't find ID_INPUT_TOUCHSCREEN=1, so the touch screen ends
    // up with a keyboard input processor.
    private LinuxInputDevice addDeviceInternal(LinuxInputDevice device, String name) {
        LinuxInputProcessor processor = null;
        if (name.endsWith("event0")) {
            // "mxckpd" at /dev/input/event0 (device.uevent.get("NAME") == "mxckpd")
            //   type 1 (EV_KEY), code 102 (KEY_HOME)
            //   type 1 (EV_KEY), code 116 (KEY_POWER)
            processor = new LinuxKeyProcessor();
        } else if (name.endsWith("event1")) {
            // "zForce-ir-touch" at /dev/input/event1 (device.uevent.get("NAME") == "zForce-ir-touch")
            //   type 3 (EV_ABS), code 0 (ABS_X)
            //   type 3 (EV_ABS), code 1 (ABS_Y)
            //   type 3 (EV_ABS), code 24 (ABS_PRESSURE)
            //   type 1 (EV_KEY), code 330 (BTN_TOUCH)
            //   type 0 (EV_SYN), code 0 (SYN_REPORT)
            processor = new LinuxSimpleTouchProcessor(device);
        }
        if (processor == null) {
            return null;
        } else {
            device.setInputProcessor(processor);
            Thread thread = new Thread(device);
            thread.setName(name);
            thread.setDaemon(true);
            thread.start();
            devices.add(device);
            return device;
        }
    }
}
