/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.sun.glass.ui.monocle;

import static com.sun.glass.ui.monocle.EPDSystem.FB_ACTIVATE_FORCE;
import static com.sun.glass.ui.monocle.EPDSystem.FB_ROTATE_UR;
import static com.sun.glass.ui.monocle.EPDSystem.TEMP_USE_AMBIENT;
import static com.sun.glass.ui.monocle.EPDSystem.UPDATE_MODE_FULL;
import static com.sun.glass.ui.monocle.EPDSystem.UPDATE_MODE_PARTIAL;
import static com.sun.glass.ui.monocle.EPDSystem.UPDATE_SCHEME_SNAPSHOT;
import static com.sun.glass.ui.monocle.EPDSystem.WAVEFORM_MODE_DU;
import static com.sun.glass.ui.monocle.EPDSystem.WAVEFORM_MODE_GC16;
import static com.sun.glass.ui.monocle.EPDSystem.WAVEFORM_MODE_GC4;
import static com.sun.glass.ui.monocle.EPDSystem.WAVEFORM_MODE_INIT;
import static com.sun.glass.ui.monocle.LinuxSystem.SEEK_SET;

import com.sun.glass.ui.monocle.EPDSystem.FbVarScreenInfo;
import com.sun.glass.ui.monocle.EPDSystem.MxcfbUpdateData;
import com.sun.glass.ui.monocle.EPDSystem.MxcfbWaveformModes;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * EPDFrameBuffer represents the Electrophoretic Display Controller (EPDC) Frame
 * Buffer. This class provides the EPDC frame buffer device API extensions in a
 * natural Java interface.
 *
 * @author John Neffenger
 */
public class EPDFrameBuffer {

    private static final int POWERDOWN_DELAY = 1000;
    private static final int BITS_TO_BYTES = 3;

    private static int updateMarker = 1;

    private final LinuxSystem system;
    private final EPDSystem driver;
    private final long fd;
    private final FbVarScreenInfo screen;
    private final MxcfbWaveformModes modes;
    private final MxcfbUpdateData update;
    private final int width;
    private final int height;
    private final int depth;

    private int xres;
    private int yres;
    private int xresVirtual;
    private int yresVirtual;
    private int xoffset;
    private int yoffset;
    private int bitsPerPixel;
    private int grayscale;
    private int activate;
    private int rotate;

    public EPDFrameBuffer(String fbDevPath) throws IOException {
        system = LinuxSystem.getLinuxSystem();
        driver = EPDSystem.getEPDDriver();

        fd = system.open(fbDevPath, LinuxSystem.O_RDWR);
        if (fd == -1) {
            throw new IOException(system.getErrorMessage());
        }

        // Default after boot:
        //  fb_var_screeninfo (160 bytes at 34888da0)
        //      xres = 600
        //      yres = 800
        //      xres_virtual = 608
        //      yres_virtual = 1792
        //      xoffset = 0
        //      yoffset = 0
        //      bits_per_pixel = 16
        //      grayscale = 0
        //      activate = 0
        //      rotate = 3
        //    
        // After setting framebuffer variable info:
        //  fb_var_screeninfo (160 bytes at 34886cc8)
        //      xres = 800
        //      yres = 600
        //      xres_virtual = 800
        //      yres_virtual = 1280
        //      xoffset = 0
        //      yoffset = 0
        //      bits_per_pixel = 16
        //      grayscale = 0
        //      activate = 0
        //      rotate = 0
        screen = new FbVarScreenInfo();
        modes = new MxcfbWaveformModes();
        update = new MxcfbUpdateData();

        getScreenInfo(screen);
        setScreenInfo(screen);
        getScreenInfo(screen);

        width = xresVirtual;
        height = yres;
        depth = bitsPerPixel;
    }

    private void printError(String context, String message, long errnum) {
        System.err.println(context + ": " + message + " (" + errnum + ")");
    }

    private void getScreenInfo(EPDSystem.FbVarScreenInfo screen) throws IOException {
        int rc = system.ioctl(fd, LinuxSystem.FBIOGET_VSCREENINFO, screen.p);
        if (rc != 0) {
            printError("Error getting framebuffer info", system.getErrorMessage(), rc);
            system.close(fd);
            throw new IOException(system.getErrorMessage());
        }

        xres = screen.getXRes(screen.p);
        yres = screen.getYRes(screen.p);
        xresVirtual = screen.getXResVirtual(screen.p);
        yresVirtual = screen.getYResVirtual(screen.p);
        xoffset = screen.getOffsetX(screen.p);
        yoffset = screen.getOffsetY(screen.p);
        bitsPerPixel = screen.getBitsPerPixel(screen.p);
        grayscale = screen.getGrayscale(screen.p);
        activate = screen.getActivate(screen.p);
        rotate = screen.getRotate(screen.p);
    }

    private void setScreenInfo(EPDSystem.FbVarScreenInfo screen) throws IOException {
        screen.setVirtualRes(screen.p, xres, 2 * yres);
        screen.setOffset(screen.p, xoffset, 0);
        screen.setBitsPerPixel(screen.p, 16);
        screen.setGrayscale(screen.p, 0);
        screen.setActivate(screen.p, FB_ACTIVATE_FORCE);
        screen.setRotate(screen.p, FB_ROTATE_UR);

        int rc = system.ioctl(fd, LinuxSystem.FBIOPUT_VSCREENINFO, screen.p);
        if (rc != 0) {
            printError("Error setting framebuffer info", system.getErrorMessage(), rc);
            system.close(fd);
            throw new IOException(system.getErrorMessage());
        }
    }

    protected void setWaveformModes(int init, int du, int gc4, int gc8, int gc16, int gc32) {
        // Sync not needed because called only in init().
        synchronized (modes) {
//            mode          preferred   i.mx507     i.mx6sl defaults

//            mode_init     INIT (0)    INIT (0)    0
//            mode_du       DU (1)      DU (1)      1
//            mode_gc4      GC4 (3)     GC4 (3)     2
//            mode_gc8      GC16 (2)    GC16 (2)    2
//            mode_gc16     GC16 (2)    GC16 (2)    2
//            mode_gc32     GC16 (2)    GC16 (2)    2
// Driver for i.mx6sl will upgrade GC16 automatically to:
//      GLD16 (Regal D) if UPDATE_MODE_FULL and compiled for Regal (not on Kobo)
//      GLR16 (Regal) if UPDATE_MODE_PARTIAL and compiled for Regal (not on Kobo)
//      GL16 (low-flash) if UPDATE_MODE_PARTIAL and comiled for optimized waveforms (yes on Kobo)
// i.mx6sl only:
//            mode_aa       GLR16 (6)   GC4 (3)     REAGL mode
//            mode_aad      GLD16 (7)   GC4 (3)     REAGL-D mode
//            mode_gl16     GL16 (5)    GC16 (2)    GL16 mode
//            mode_a2       A2 (4)      DU (1)      A2 mode
            modes.setModes(modes.p, init, du, gc4, gc8, gc16, gc32);
            int rc = system.ioctl(fd, driver.MXCFB_SET_WAVEFORM_MODES, modes.p);
            if (rc != 0) {
                printError("Error setting waveform modes", system.getErrorMessage(), rc);
            }
        }
    }

    protected void setUpdateScheme(int scheme) {
        int rc = driver.ioctl(fd, driver.MXCFB_SET_UPDATE_SCHEME, scheme);
        if (rc != 0) {
            printError("Error setting update scheme", system.getErrorMessage(), rc);
        }
    }

    protected void setPowerdownDelay(int delay) {
        int rc = driver.ioctl(fd, driver.MXCFB_SET_PWRDOWN_DELAY, delay);
        if (rc != 0) {
            printError("Error setting power down delay", system.getErrorMessage(), rc);
        }
    }

    protected void waitForUpdateComplete(int marker) {
        int rc = driver.ioctl(fd, driver.MXCFB_WAIT_FOR_UPDATE_COMPLETE, marker);
        if (rc < 0) {
            printError("Error waiting for display update", system.getErrorMessage(), rc);
        }
    }

    protected int sendUpdate(int updateMode, int waveformMode, int flags) {
        // Sync not needed because called only from JavaFX application thread.
        synchronized (update) {
            int marker = updateMarker++;
            update.setUpdateRegion(update.p, 0, 0, xres, yres);
            update.setWavevformMode(update.p, waveformMode);
            update.setUpdateMode(update.p, updateMode);
            update.setUpdateMarker(update.p, marker);
            update.setTemp(update.p, TEMP_USE_AMBIENT);
            update.setFlags(update.p, flags);
            int rc = system.ioctl(fd, driver.MXCFB_SEND_UPDATE, update.p);
            if (rc != 0) {
                printError("Error sending display update", system.getErrorMessage(), rc);
            }
            return marker;
        }
    }

    protected void fill(byte filler) {
        ByteBuffer bb = ByteBuffer.allocateDirect(width * height * (depth >>> BITS_TO_BYTES));
        bb.order(ByteOrder.nativeOrder());
        for (int i = 0; i < bb.limit(); i++) {
            bb.put((byte) filler);
        }

        bb.flip();
        long offset = system.lseek(fd, 0, SEEK_SET);
        if (offset == -1) {
            printError("Error resetting framebuffer write offset", system.getErrorMessage(), offset);
        }
        long numBytes = system.write(fd, bb, bb.position(), bb.limit());
        if (numBytes == -1) {
            printError("Error writing to framebuffer", system.getErrorMessage(), numBytes);
        }
    }

    protected void init() throws IOException {
        setWaveformModes(WAVEFORM_MODE_INIT, WAVEFORM_MODE_DU, WAVEFORM_MODE_GC4,
                WAVEFORM_MODE_GC16, WAVEFORM_MODE_GC16, WAVEFORM_MODE_GC16);
        setUpdateScheme(UPDATE_SCHEME_SNAPSHOT);
        setPowerdownDelay(POWERDOWN_DELAY);
        fill((byte) 0xFF);
        int marker = sendUpdate(UPDATE_MODE_FULL, WAVEFORM_MODE_DU, 0);
        waitForUpdateComplete(marker);
    }

    public int sendUpdate() {
        return sendUpdate(UPDATE_MODE_PARTIAL, WAVEFORM_MODE_GC16, 0);
    }

    public void waitUpdate(int marker) {
        if (marker > 0) {
            waitForUpdateComplete(marker);
        }
    }

    public void sync() {
        int marker = sendUpdate(UPDATE_MODE_PARTIAL, WAVEFORM_MODE_GC16, 0);
        waitForUpdateComplete(marker);
    }

    public void close() {
        system.close(fd);
    }

    public long getNativeHandle() {
        return fd;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }
}
