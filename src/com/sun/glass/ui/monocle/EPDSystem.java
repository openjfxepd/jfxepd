/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.sun.glass.ui.monocle;

import com.sun.glass.utils.NativeLibLoader;
import java.security.Permission;

/**
 * EPDSystem provides access to the Mobile eXtreme Convergence (MXC)
 * Electrophoretic Display Controller (EPDC) Frame Buffer (FB) driver
 * extensions. The frame buffer name is "mxc_epdc_fb" at "/dev/fb0". The
 * C-language extensions to the standard frame buffer device API are defined in
 * the include file "mxcfb.h". This class provides the structures and constants
 * of "mxcfb.h" in Java.
 *
 * The driver source code is "linux-2.6.35.3/drivers/video/mxc/mxc_epdc_fb.c".
 *
 * This class also extends LinuxSystem.FbVarScreenInfo to provide the rest of
 * the fields found in fb_var_screeninfo (linux/fb.h).
 *
 * EPDSystem is a singleton. Its instance is obtained by calling
 * EPDSystem.getEPDDriver().
 *
 * @author John Neffenger
 */
public class EPDSystem {

    private static final int SIZEOF_INT = 4;

    private static final Permission permission = new RuntimePermission("loadLibrary.*");

    private static final EPDSystem instance = new EPDSystem();

    private final LinuxSystem system;

    // IOCTLs for E-ink panel updates
    int MXCFB_SET_WAVEFORM_MODES;
    int MXCFB_SET_TEMPERATURE;
    int MXCFB_SET_AUTO_UPDATE_MODE;
    int MXCFB_SEND_UPDATE;
    int MXCFB_WAIT_FOR_UPDATE_COMPLETE;
    int MXCFB_WAIT_FOR_UPDATE_COMPLETE2;
    int MXCFB_SET_PWRDOWN_DELAY;
    int MXCFB_GET_PWRDOWN_DELAY;
    int MXCFB_SET_UPDATE_SCHEME;
    int MXCFB_SET_MERGE_ON_WAVEFORM_MISMATCH;

    /**
     * Obtains the single instance of EPDSystem. Calling this method requires
     * the RuntimePermission "loadLibrary.*". loadLibrary() must be called on
     * the EPDSystem instance before any system calls can be made using it.
     */
    static EPDSystem getEPDDriver() {
        checkPermissions();
        return instance;
    }

    private static void checkPermissions() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkPermission(permission);
        }
    }

    private EPDSystem() {
        system = LinuxSystem.getLinuxSystem();
    }

    /**
     * Loads native libraries required to make system calls using EPDSystem
     * methods. This method must be called before any other instance methods of
     * EPDSystem. If this method is called multiple times, it has no effect
     * after the first call.
     */
    void loadLibrary() {
        NativeLibLoader.loadLibrary("glass_monocle_epd");

        MxcfbWaveformModes modes = new EPDSystem.MxcfbWaveformModes();
        MxcfbUpdateData update = new EPDSystem.MxcfbUpdateData();

        MXCFB_SET_WAVEFORM_MODES = system.IOW('F', 0x2B, modes.sizeof());
        MXCFB_SET_TEMPERATURE = system.IOW('F', 0x2C, SIZEOF_INT);
        MXCFB_SET_AUTO_UPDATE_MODE = system.IOW('F', 0x2D, SIZEOF_INT);
        MXCFB_SEND_UPDATE = system.IOW('F', 0x2E, update.sizeof());
        MXCFB_WAIT_FOR_UPDATE_COMPLETE = system.IOW('F', 0x2F, SIZEOF_INT);
        MXCFB_SET_PWRDOWN_DELAY = system.IOW('F', 0x30, SIZEOF_INT);
        MXCFB_GET_PWRDOWN_DELAY = system.IOR('F', 0x31, SIZEOF_INT);
        MXCFB_SET_UPDATE_SCHEME = system.IOW('F', 0x32, SIZEOF_INT);
        MXCFB_SET_MERGE_ON_WAVEFORM_MISMATCH = system.IOW('F', 0x37, SIZEOF_INT);
    }

    native int ioctl(long fd, int request, int value);

    // linux/fb.h
    static final int FB_ACTIVATE_FORCE = 128;

    // mxcfb.h
    // Grayscale values
    static final int GRAYSCALE_8BIT = 0x1;
    static final int GRAYSCALE_8BIT_INVERTED = 0x2;

    // Auto-update methods
    static final int AUTO_UPDATE_MODE_REGION_MODE = 0;
    static final int AUTO_UPDATE_MODE_AUTOMATIC_MODE = 1;

    // Update schemes
    static final int UPDATE_SCHEME_SNAPSHOT = 0;
    static final int UPDATE_SCHEME_QUEUE = 1;
    static final int UPDATE_SCHEME_QUEUE_AND_MERGE = 2;

    /**
     * Applying the waveform to only the pixels that change in a given region
     */
    static final int UPDATE_MODE_PARTIAL = 0x0;

    /**
     * Full—Applying a waveform to all pixels in a give region (Often confused
     * with Global)
     */
    static final int UPDATE_MODE_FULL = 0x1;

    static final int WAVEFORM_MODE_AUTO = 257;

    // Temperatures
    static final int TEMP_USE_AMBIENT = 0x1000;

    // Flags
    static final int EPDC_FLAG_ENABLE_INVERSION = 0x01;
    static final int EPDC_FLAG_FORCE_MONOCHROME = 0x02;
    static final int EPDC_FLAG_USE_ALT_BUFFER = 0x100;

    static final int FB_POWERDOWN_DISABLE = -1;
    static final int FB_TEMP_AUTO_UPDATE_DISABLE = -1;

    /**
     * Initialization waveform (0-F → F in ~4000 ms). Clears the screen to all
     * white.
     * <p>
     * “A first exemplary drive scheme provides waveforms that may be used to
     * change the display state of a pixel from any initial display state to a
     * new display state of white. The first drive scheme may be referred to as
     * an initialization or ‘INIT’ drive scheme.”
     * <i>—United States Patent 9,280,955</i>
     */
    static final int WAVEFORM_MODE_INIT = 0;

    /**
     * Direct Update waveform (0-F → 0 or F in ~260 ms). Changes any gray values
     * to either black or white.
     * <p>
     * “A second exemplary drive scheme provides waveforms that may be used to
     * change the display state of a pixel from any initial display state to a
     * new display state of either white or black. The second drive scheme may
     * be referred to as a ‘DU’ drive scheme.”
     * <i>—United States Patent 9,280,955</i>
     */
    static final int WAVEFORM_MODE_DU = 1;

    /**
     * Gray 16-level waveform (0-F → 0-F in ~760 ms). Supports 4-bit grayscale
     * images and text with high quality.
     * <p>
     * “A fourth exemplary drive scheme provides waveforms that may be used to
     * change the display state of a pixel from any initial display state to a
     * new display state. The initial state may be any four-bit (16 gray states)
     * value. The new display state may be any four-bit (16 gray states) value.
     * The fourth drive scheme may be referred to as a ‘GC16’ drive scheme.”
     * <i>—United States Patent 9,280,955</i>
     */
    static final int WAVEFORM_MODE_GC16 = 2;

    /**
     * Gray 4-level waveform (0-F → 0, 5, A, or F in ~500 ms). Supports 2-bit
     * grayscale images and text with lower quality.
     * <p>
     * “A third exemplary drive scheme provides waveforms that may be used to
     * change the display state of a pixel from any initial display state to a
     * new display state. The initial state may be any four-bit (16 gray states)
     * value. The new display state may be any two-bit (4 gray states) value.
     * The third drive scheme may be referred to as a ‘GC4’ drive scheme.”
     * <i>—United States Patent 9,280,955</i>
     */
    static final int WAVEFORM_MODE_GC4 = 3;

    /**
     * Application update waveform (0 or F → 0 or F in ~120 ms). A fast 1-bit
     * black and white animation mode at about 8 frames per second.
     * <p>
     * “A fifth exemplary drive scheme provides waveforms that may be used to
     * change the display state of a pixel from an initial display state to a
     * new display state. The initial state must be white or black. The new
     * display state may be black or white. The fifth drive scheme may be
     * referred to as an ‘A2’ drive scheme. An advantage of A2 waveforms is that
     * they have generally short waveform periods, providing rapid display
     * updates. A disadvantage of A2 waveforms is that there use may result in
     * ghosting artifacts.”
     * <i>—United States Patent 9,280,955</i>
     */
    static final int WAVEFORM_MODE_A2 = 4;

    /**
     * Gray low-flash waveform good for e-readers (4-bit grayscale).
     * <p>
     * The Kobo i.MX6SL driver automatically upgrades the
     * {@link #WAVEFORM_MODE_GC16} waveform to this one for
     * {@link #UPDATE_MODE_PARTIAL} updates if this waveform is supported on the
     * device and the preferred Regal {@link #WAVEFORM_MODE_GLR16} waveform is
     * not available.
     */
    static final int WAVEFORM_MODE_GL16 = 5;

    /**
     * <em>Regal</em> low-flash drive scheme with low edge artifacts (4-bit
     * grayscale).
     * <p>
     * The Kobo i.MX6SL driver automatically upgrades the
     * {@link #WAVEFORM_MODE_GC16} waveform to this one for
     * {@link #UPDATE_MODE_PARTIAL} updates if Regal waveforms are supported on
     * the device.
     */
    static final int WAVEFORM_MODE_GLR16 = 6;

    /**
     * <em>Regal D</em> no-flash drive scheme with low edge artifacts (4-bit
     * grayscale with dithering).
     * <p>
     * The Kobo i.MX6SL driver automatically upgrades the
     * {@link #WAVEFORM_MODE_GC16} waveform to this one for
     * {@link #UPDATE_MODE_FULL} updates if Regal waveforms are supported on the
     * device.
     */
    static final int WAVEFORM_MODE_GLD16 = 7;

    static class IntegerStructure extends C.Structure {

        IntegerStructure() {
            checkPermissions();
        }

        @Override
        native int sizeof();

        native int getInteger(long p);

        native void setInteger(long p, int integer);
    }

    // linux/fb.h
    static final int FB_ROTATE_UR = 0;
    static final int FB_ROTATE_CW = 1;
    static final int FB_ROTATE_UD = 2;
    static final int FB_ROTATE_CCW = 3;

    /**
     * FbVarScreenInfo wraps all of the C structure fb_var_screeninfo, defined
     * in linux/fb.h.
     */
    static class FbVarScreenInfo extends LinuxSystem.FbVarScreenInfo {

        FbVarScreenInfo() {
            super();
        }

        native int getGrayscale(long p);

        native int getRedOffset(long p);

        native int getRedLength(long p);

        native int getRedMsbRight(long p);

        native int getGreenOffset(long p);

        native int getGreenLength(long p);

        native int getGreenMsbRight(long p);

        native int getBlueOffset(long p);

        native int getBlueLength(long p);

        native int getBlueMsbRight(long p);

        native int getTranspOffset(long p);

        native int getTranspLength(long p);

        native int getTranspMsbRight(long p);

        native int getNonstd(long p);

        native int getActivate(long p);

        native int getHeight(long p);

        native int getWidth(long p);

        native int getAccelFlags(long p);

        native int getPixclock(long p);

        native int getLeftMargin(long p);

        native int getRightMargin(long p);

        native int getUpperMargin(long p);

        native int getLowerMargin(long p);

        native int getHsyncLen(long p);

        native int getVsyncLen(long p);

        native int getSync(long p);

        native int getVmode(long p);

        native int getRotate(long p);

        native int getColorspace(long p);

        native void setGrayscale(long p, int grayscale);

        native void setNonstd(long p, int nonstd);

        native void setHeight(long p, int height);

        native void setWidth(long p, int width);

        native void setAccelFlags(long p, int accelFlags);

        native void setPixclock(long p, int pixclock);

        native void setLeftMargin(long p, int leftMargin);

        native void setRightMargin(long p, int rightMargin);

        native void setUpperMargin(long p, int upperMargin);

        native void setLowerMargin(long p, int lowerMargin);

        native void setHsyncLen(long p, int hsyncLen);

        native void setVsyncLen(long p, int vsyncLen);

        native void setSync(long p, int sync);

        native void setVmode(long p, int vmode);

        native void setRotate(long p, int rotate);

        native void setColorspace(long p, int colorspace);
    }

    /**
     * MxcfbWaveformModes wraps the C structure mxcfb_waveform_modes, defined in
     * mxcfb.h.
     */
    static class MxcfbWaveformModes extends C.Structure {

        MxcfbWaveformModes() {
            checkPermissions();
        }

        @Override
        native int sizeof();

        native int getModeInit(long p);

        native int getModeDu(long p);

        native int getModeGc4(long p);

        native int getModeGc8(long p);

        native int getModeGc16(long p);

        native int getModeGc32(long p);

        native void setModes(long p, int init, int du, int gc4, int gc8, int gc16, int gc32);

        native void print(long p);
    }

    /**
     * MxcfbUpdateData wraps the C structure mxcfb_update_data, defined in
     * mxcfb.h.
     */
    static class MxcfbUpdateData extends C.Structure {

        MxcfbUpdateData() {
            checkPermissions();
        }

        @Override
        native int sizeof();

        native int getUpdateRegionTop(long p);

        native int getUpdateRegionLeft(long p);

        native int getUpdateRegionWidth(long p);

        native int getUpdateRegionHeight(long p);

        native int getWaveformMode(long p);

        native int getUpdateMode(long p);

        native int getUpdateMarker(long p);

        native int getTemp(long p);

        native int getFlags(long p);

        native long getAltBufferDataVirtAddr(long p);

        native long getAltBufferDataPhysAddr(long p);

        native int getAltBufferDataWidth(long p);

        native int getAltBufferDataHeight(long p);

        native int getAltBufferDataUpdateRegionTop(long p);

        native int getAltBufferDataUpdateRegionLeft(long p);

        native int getAltBufferDataUpdateRegionWidth(long p);

        native int getAltBufferDataUpdateRegionHeight(long p);

        native void setUpdateRegion(long p, int top, int left, int width, int height);

        native void setWavevformMode(long p, int mode);

        native void setUpdateMode(long p, int mode);

        native void setUpdateMarker(long p, int marker);

        native void setTemp(long p, int temp);

        native void setFlags(long p, int flags);

        native void setAltBufferData(long p, long virtAddr, long physAddr, int width, int height,
                int updateRegionTop, int updateRegionLeft, int updateRegionWidth, int updateRegionHeight);

        native void print(long p);
    }
}
